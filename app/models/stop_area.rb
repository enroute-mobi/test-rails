class StopArea < ApplicationRecord
  belongs_to :parent, class_name: "StopArea", optional: true

  validates :name, presence: true

  attribute :centroid, :st_point, srid: 4326, geographic: true
end
