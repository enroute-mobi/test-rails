desc "Import given (GTFS) file"
task :import, [:file] do |t, args|
  import_duration = Benchmark.realtime do
    Import::Gtfs.new(args[:file]).import
  end
  puts "Import performed in #{import_duration} ms"
end

namespace :import do
  desc "Clean all models"
  task :clean do
    StopArea.delete_all
  end
end

namespace :download do
  desc "Download a large GTFS sample"
  task :big_sample do
    sh "curl -L -o gtfs.zip https://transitfeeds.com/p/entur/970/latest/download"
  end

  desc "Download a small GTFS sample"
  task :small_sample do
    sh "curl -L -o gtfs.zip https://bitbucket.org/enroute-mobi/test-rails/downloads/sample-feed-with-parent.zip"
  end
end
